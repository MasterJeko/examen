import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Student {

    Scanner scanner = new Scanner(System.in);
    String answer = null;
    String name = null;
    String lastName = null;
    String group = null;
    double idnp = 0;
    int nota1 = 0;
    int nota2 = 0;
    int notaExamen = 0;
    double sum = 0;

    private Connection connect = null;
    private String dataBaseURL = "jdbc:mysql://localhost:3306/students";
    private String user = "root";
    private String password = "root";

    public String registerNewStudent() {
        System.out.println("Doriti sa inregistrati un student nou? Da/Nu");
        answer = scanner.nextLine();
        if (answer.equals("Da")) {
            System.out.println("Introdu nume student: ");
            name = scanner.next();
            System.out.println("Introdu prenume student: ");
            lastName = scanner.next();
            System.out.println("Introdu grupa student: ");
            group = scanner.next();
            System.out.println("Introdu IDNP student: ");
            idnp = scanner.nextDouble();
            System.out.println("Introdu nota test1: ");
            nota1 = scanner.nextInt();
            System.out.println("Introdu nota tes2: ");
            nota2 = scanner.nextInt();
            System.out.println("Introdu nota examen: ");
            notaExamen = scanner.nextInt();
            String sql = "insert into table_name (nume) values ('" + name + "')";

            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
                connect = DriverManager.getConnection(dataBaseURL, user, password);
                Statement statement = connect.createStatement();
                statement.executeUpdate(sql);
            } catch (SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("La revedere");
        }

        mediaAritmetica();

        return answer;
    }

    public double mediaAritmetica() {
        System.out.println("Doriti sa aflati media aritmetica? Da/Nu");
        answer = scanner.next();
        if (answer.equals("Da")) {
            sum = ((nota1 + nota2 + notaExamen)) / 3;
            System.out.println("Media aritemetica este: " + sum);
        } else {
            System.out.println("La revedere");
        }
        return sum;
    }

    public void infoStudent() {
        registerNewStudent();
        System.out.println("Doriti sa afisati info student? Da/nu");
        answer = scanner.next();
        if (answer.equals("Da")) {
            System.out.println("Nume: " + name + "Prenume: " + lastName + "Grupa: " + group + "IDNP: " + idnp
                    + "Nota test 1: " + nota1 + "Nota test 2: " + nota2 + "Nota Examen: " + notaExamen);
        } else {
            System.out.println("La revedere");
        }
    }
}
